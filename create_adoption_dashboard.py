#!/usr/bin/env python3

import gitlab
import json
import os
import argparse
import requests
import datetime
import yaml
import collections
from mako.template import Template

# creating a list of paths in the CI yml file, so that I can find a specific one
# not looking for values of "script" and "rules" part
def make_path_list(json, pathlist = [], parent = ""):
    exclusion_list = ["script", "rules", "before_script", "after_script", "services", "parallel", "cache", "include", "dependencies"]
    if isinstance(json, dict):
        for key in json.keys():
            path = key
            if parent:
                path = parent + ":" + key
            if path not in pathlist:
                pathlist.append(path)
            if key not in exclusion_list:
                make_path_list(json[key], pathlist, path)
    else:
        if isinstance(json, list):
            for value in json:
                if isinstance(value, list) or isinstance(value, dict):
                    for array_value in value:
                        path = parent + ":" + array_value
                        pathlist.append(path)
                else:
                    path = parent + ":" + value
                    pathlist.append(path)
        else:
            path = parent + ":" + str(json)
            pathlist.append(path)
    return pathlist

def get_merged_yml(parsed_ci_content, args):
    data = {'content': parsed_ci_content}
    headers = {'PRIVATE-TOKEN': args.token}
    gitlab = args.gitlab.strip("/")
    try:
        response = requests.post( args.gitlab + "/api/v4/ci/lint?include_merged_yaml=true", headers = headers, data = data)
        response_body = response.json()
        if response_body["status"] == "valid":
            return response_body["merged_yaml"]
        else:
            print("YML invalid")
            return None
    except Exception as err:
        print("Could not lint CI file:")
        print(err)
        return None

def get_group_projects(gl, groups):
    projects = []
    for topgroup in groups:
        group = gl.groups.get(topgroup)
        group_projects = group.projects.list(as_list=False, include_subgroups=True)
        for group_project in group_projects:
            projects.append(group_project.attributes["id"])
    return projects

def check_project_adoption(project, ci_yml_pathlist, adoption_criteria):
    adoption = {}
    for adoption_key in adoption_criteria:
        adoption_criterium = adoption_criteria[adoption_key]
        found = False
        for ci_keyword_path in ci_yml_pathlist:
            # here we check if the path of CI keywords we want to be adopted is in the CI file
            if adoption_criterium["path"] in ci_keyword_path:
                found = True
                break
        adoption["a_" + adoption_key] = int(found)
    return adoption

def sort_by_group(project_adoptions):
    # sort groups alphabetically, sort projects in groups alphabetically, produce a list
    sorted_projects = {}
    for project in project_adoptions:
        project_group = project["path"]
        project_group = project_group[0:project_group.rfind("/")]
        project["group"] = project_group
        if project_group in sorted_projects:
            sorted_projects[project_group].append(project)
            sorted_projects[project_group] = sorted(sorted_projects[project_group], key=lambda s: s["name"])
        else:
            sorted_projects[project_group] = [project]
    sorted_projects = collections.OrderedDict(sorted(sorted_projects.items()))
    project_list = []
    for group in sorted_projects:
        projects = sorted_projects[group]
        for project in projects:
            project_list.append(project)
    return project_list

def get_max_adoption(project_adoptions):
    max_adoption = {}
    for project in project_adoptions:
        project_adoption = {}
        if project["date"] not in max_adoption:
            max_adoption[project["date"]] = {}
        for key in project.keys():
            if key.startswith("a_"):
                if key in max_adoption[project["date"]]:
                    max_adoption[project["date"]][key] += project[key]
                else:
                    max_adoption[project["date"]][key] = project[key]
    max_value = 0
    for adoption_values in max_adoption.values():
        for adoption_value in adoption_values.values():
            if adoption_value > max_value:
                max_value = adoption_value
    return max_value

def get_group_count(project_adoptions):
    groups = set()
    for project in project_adoptions:
        groups.add(project["group"])
    return len(groups)

def get_adoption_report(gl, projects, adoption_criteria, date, args):
    project_adoptions = []
    pathlists = []
    for project_id in projects:
        print("[Info] Checking adoption of project %s" % project_id)
        project = gl.projects.get(project_id)
        has_ci = False
        if "ci_config_path" in project.attributes:
            ci_config_path = project.attributes["ci_config_path"] if project.attributes["ci_config_path"] else '.gitlab-ci.yml'
            ci_file_content = ""
            try:
                ci_file = project.files.get(file_path=ci_config_path, ref=project.default_branch)
                ci_file_content = ci_file.decode()
                has_ci = True
            except Exception as e:
                print("[ERROR] Could not get CI file '%s' for project %s" % (ci_config_path, project.attributes["path_with_namespace"]))
                print(e)
            parsed_ci = {}
            if ci_file_content:
                try:
                    merged_yml = get_merged_yml(ci_file_content, args)
                    parsed_ci = yaml.safe_load(merged_yml)
                except Exception as e:
                    print("[ERROR] Could not get merged yaml for project %s" % (project.attributes["path_with_namespace"]))
                    has_ci = False
                    print(e)
            pathlist = []
            if parsed_ci:
                try:
                    pathlist = make_path_list(parsed_ci, pathlist)
                except Exception as e:
                    print("[ERROR] Could not parse keys for project %s" % (project.attributes["path_with_namespace"]))
                    has_ci = False
                    print(e)
        else:
            pathlist = []
        project_adoption = {}
        project_adoption["name"] = project.name
        project_adoption["path"] = project.attributes["path_with_namespace"]
        project_adoption["url"] = project.attributes["web_url"]
        project_adoption["has_ci"] = int(has_ci)
        project_adoption["date"] = date
        project_adoption.update(check_project_adoption(project, pathlist, adoption_criteria))
        project_adoptions.append(project_adoption)
        project_paths = {}
        project_paths["name"] = project.name
        project_paths["path"] = project.attributes["path_with_namespace"]
        project_paths["url"] = project.attributes["web_url"]
        project_paths["paths"] = pathlist
        pathlists.append(project_paths)
    return project_adoptions, pathlists

def align_criteria(existing_data, adoption_criteria):
    aligned_data = []
    for data in existing_data:
        for criterium in adoption_criteria:
            if "a_" + criterium not in data:
                print("Missing %s in %s, setting to 0." % (criterium, data))
                data["a_" + criterium] = 0
        aligned_data.append(data)
    return aligned_data

parser = argparse.ArgumentParser(description='Create report for GitLab issues')
parser.add_argument('token', help='API token able to read the requested projects')
parser.add_argument('configfile', help='YML file that defines requested groups, projects and adoption parameters')
parser.add_argument('--gitlab', help='URL of the gitlab instance', default="https://gitlab.com/")

args = parser.parse_args()

gl = gitlab.Gitlab(args.gitlab, private_token=args.token)
date = datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")

configfile = args.configfile
groups = []
projects = []
adoption_criteria = {}

with open(configfile, "r") as c:
    config = yaml.load(c, Loader=yaml.FullLoader)
    if "groups" in config:
        groups = config["groups"]
        projects = get_group_projects(gl, groups)
    else:
        if not "projects" in config:
            print("Error: No group or project configured. Stopping.")
            exit(1)
        else:
            projects = config["projects"]

    if "adoption_criteria" in config:
        adoption_criteria = config["adoption_criteria"]
    else:
        print("No adoption_criteria provided, exiting")

project_adoptions = get_adoption_report(gl, projects, adoption_criteria, date, args)
pathlist = project_adoptions[1]
project_adoptions = sort_by_group(project_adoptions[0])

# make sure we have the data folder
if not os.path.exists('data'):
    os.mkdir('data')

if not os.path.isfile("data/adoption_data.json"):
    with open("data/adoption_data.json", "w") as reportfile:
        json.dump(project_adoptions, reportfile, indent = 2)
else:
    existing_data = []
    with open("data/adoption_data.json", "r") as reportfile:
        existing_data = json.load(reportfile)
        existing_data = align_criteria(existing_data, adoption_criteria)
    project_adoptions.extend(existing_data)
    with open("data/adoption_data.json", "w") as reportfile:
        json.dump(project_adoptions, reportfile, indent = 2)

timeline_ticks = get_max_adoption(project_adoptions)
# after adding the new crawl, pull previous data and merge to common data file
# merge so that for each project, there will be a list of adoption entries, each with the appropriate date

mytemplate = Template(filename='template/index.html')

adoption_criteria = [ "a_"+crit for crit in adoption_criteria.keys()]

with open("public/ci_paths.json", "w") as pathfile:
    json.dump(pathlist, pathfile, indent = 2)

with open("public/index.html","w") as outfile:
    outfile.write(mytemplate.render(projects = project_adoptions,
        project_adoption = "adoption_data.json",
        date = date,
        criteria = adoption_criteria,
        gitlab = args.gitlab.strip("/"),
        timeline_ticks = timeline_ticks,
        group_count = get_group_count(project_adoptions)))
